'use strict';

document.addEventListener('DOMContentLoaded', () => {
    //carousel

    const slides = document.querySelectorAll('.carousel__slide'),
        prev = document.querySelector('.carousel__counter-left'),
        next = document.querySelector('.carousel__counter-right'),
        slidesWrapper = document.querySelector('.carousel__wrapper'),
        slidesField = document.querySelector('.carousel__inner'),
        width = window.getComputedStyle(slidesWrapper).width;
    
    let offset = 0;

    slidesField.style.width = 100 * slides.length + '%';
    slidesField.style.display = 'flex';
    slidesField.style.transition = '0.5s all';

    slidesWrapper.style.overflow = 'hidden';

    slides.forEach(slide => {
        slide.style.width = width;
    });

    function deleteNotDigits (str) {
        return +str.replace(/\D/g, '');
    }

    next.addEventListener('click', () => {
        if (offset == deleteNotDigits(width) * (slides.length - 1)) {
            offset = 0;
        } else {
            offset += deleteNotDigits(width);
        }
        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    prev.addEventListener('click', () => {
        if (offset == 0) {          
            offset = deleteNotDigits(width) * (slides.length - 1);
        } else {
            offset -= deleteNotDigits(width);
        }
        slidesField.style.transform = `translateX(-${offset}px)`;
    });


    //tabs

    const tabs = document.querySelectorAll('.catalog__tab'),
        tabsContent = document.querySelectorAll('.catalog__content'),
        tabsParent = document.querySelector('.catalog__tabs');

        function hideTabContent () {
            tabsContent.forEach(item => {
                item.classList.add('hide');
                item.classList.remove('showcase', 'fade');
            });

            tabs.forEach(item => {
                item.classList.remove('catalog__tab_active');
            });
        }


        function showTabContent(i = 0) {
            tabsContent[i].classList.add('showcase', 'fade');
            tabsContent[i].classList.remove('hide');
            tabs[i].classList.add('catalog__tab_active');
        }

        hideTabContent();
        showTabContent();

        tabsParent.addEventListener('click', (e) => {
            const target = e.target;

            if (target && target.classList.contains('catalog__tab')) {
                tabs.forEach((item, i) => {
                    if (target == item) {
                        hideTabContent();
                        showTabContent(i);
                    }
                });
            }
        });

    //modal

    const modalTriggerConsult = document.querySelectorAll('[data-modal="consultation"]'),
        modalConsult = document.querySelector('#consultation');

    modalTriggerConsult.forEach(btn => {
        btn.addEventListener('click', () => {
        modalConsult.classList.add('show');
        modalConsult.classList.remove('hide');
        document.body.style.overflow = 'hidden';
        });
    });

     
    function closeModalConsult() {
        modalConsult.classList.add('hide');
        modalConsult.classList.remove('show');
        document.body.style.overflow = '';
    }

    modalConsult.addEventListener('click', (e) => {
        if (e.target === modalConsult || e.target.getAttribute('data-close') == "") {
            closeModalConsult();
        }
    });

    //buy

    const modalTriggerBuy = document.querySelectorAll('.button_mini'),
        modalOrder = document.querySelector('#order');


    modalTriggerBuy.forEach(btn => {
        btn.addEventListener('click', () => {
        modalOrder.classList.add('show');
        modalOrder.classList.remove('hide');
        document.body.style.overflow = 'hidden';
        });
    });
        
    function closeModalBuy() {
        modalOrder.classList.add('hide');
        modalOrder.classList.remove('show');
        document.body.style.overflow = '';
    }

    modalOrder.addEventListener('click', (e) => {
        if (e.target === modalOrder || e.target.getAttribute('data-close') == "") {
            closeModalBuy();
        }
    });  


    // function bindModal (triggerSelector, modalSelector, closeSelector) {

    //     const trigger = document.querySelectorAll(triggerSelector),
    //     modal = document.querySelector(modalSelector),
    //     close = document.querySelector(closeSelector);

    //     trigger.forEach(item => {
    //         item.addEventListener('click', () => {
    //             modal.style.display = "block";
    //             document.body.style.overflow = "hidden";
    //         });
    //     });

    //     close.addEventListener('click', () => {
    //         modal.style.display = "none";
    //         document.body.style.overflow = "";
    //     });

    //     modal.addEventListener('click', (e) => {
    //         if(e.target === modal) {
    //             modal.style.display = "none";
    //             document.body.style.overflow = "";
    //         }
    //     });

    // }

    //  bindModal('[data-modal="consultation"]', '#consultation', '.modal__close_consultation');
    //  bindModal('.button_mini', '#order', '.modal__close_order'); 

    
    //  nazvanie tovara 

    const modalDescr = document.querySelector('.modal__descr_order'),
        catalogSubtitle = document.querySelectorAll('.catalog-item__subtitle');

    modalTriggerBuy.forEach((btn, j)=> {
        btn.addEventListener('click' , () => {
                modalDescr.textContent = catalogSubtitle[j].textContent;
                modalOrder.classList.add('show');  
        });
    });

    //podrobnee
     
    const more = document.querySelectorAll('.catalog-item__link'),
    back = document.querySelectorAll('.catalog-item__back'),
    content = document.querySelectorAll('.catalog-item__content'),
    list = document.querySelectorAll('.catalog-item__list');


    function showMore (n = 0) {
        content[n].classList.remove('catalog-item__content_active');
        list[n].classList.add('catalog-item__list_active');
    }

    more.forEach((link, n) => {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            showMore(n);
        });
    });

    function backToMain (n = 0) {
        list[n].classList.remove('catalog-item__list_active');
        content[n].classList.add('catalog-item__content_active');
        
    }

    back.forEach((link, n) => {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            backToMain(n);
        });
    });

    //otpravka formu

    const forms = document.querySelectorAll('form');

    const message = {
        loading: 'Загрузка',
        success: 'Спасибо! скоро мы с вами свяжемся',
        failure: 'Что-то пошло не так...'
    };

    forms.forEach(item => {
        postData(item);
    });


    function postData(form) {
        form.addEventListener ('submit', (e) => {
            e.preventDefault();

            const statusMessage = document.createElement('div');
            statusMessage.classList.add('status');
            statusMessage.textContent = message.loading;
            form.append(statusMessage);


            const formData = new FormData(form);

            const object = {};
            formData.forEach(function(value, key) {
                object[key] = value;
            });

            fetch('server.php', {
                method: "POST",
                headers: { 'Content-type': 'application/json'
                },
                body: JSON.stringify(object)
            })
            .then(data => data.text())
            .then(data => {
                console.log(data);
                showThanksModal();
                statusMessage.remove();
            })
            .catch(() => {
                statusMessage.textContent = message.failure;
            })
            .finally(() => {
                form.reset();
            });

        });
    }

    //thanks
    const modalThanks = document.querySelector('#thanks');

    function showThanksModal() {
        modalConsult.classList.add('hide');
        modalOrder.classList.add('hide');
        
        modalThanks.classList.add('show');
 
        setTimeout(() => {
            modalThanks.remove();
            modalConsult.classList.remove('hide');
            modalOrder.classList.remove('hide');
            modalConsult.classList.add('show');
            modalOrder.classList.add('show');
            closeModalConsult();
            closeModalBuy();
        },3000);
    }

}); 